// Third task
const buttons = document.querySelectorAll(".btn");

let timer;

buttons.forEach((elem) => {
  elem.addEventListener("click", () => {
    if (timer !== undefined) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      alert(elem.id);
    }, 2000);
  });
});
