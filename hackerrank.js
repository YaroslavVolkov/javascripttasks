// HackerRank
const nums = [1, 4, 6, 6, 5];
function getSecondLargest(nums) {
  nums.sort(function (a, b) {
    return a - b;
  });
  const largestNum = nums[nums.length - 1];
  for (let i = nums.length - 2; i >= 0; i--) {
    if (nums[i] < largestNum) {
      return nums[i];
    }
  }
}

console.log(getSecondLargest(nums));
