"use strict";

// User genetaror
let possibleNames = [
  "John",
  "Alex",
  "Bill",
  "Joshua",
  "Alexander",
  "Alisa",
  "Elena",
  "Stacey",
];

const randomName = (names) => {
  let rand = Math.floor(Math.random() * names.length);
  return names[rand];
};

const randomAge = (min, max) => {
  max = Math.floor(max);
  min = Math.ceil(min);
  return Math.floor(Math.random() * (max - min + 1) + min);
};

function usersGenerator() {
  let userArray = [];
  let id = 1;
  let userData;
  for (let i = 0; i < 100; i++) {
    userData = {};
    userData.name = randomName(possibleNames);
    userData.age = randomAge(18, 40);
    userData.id = id;
    id++;
    userArray.push(userData);
  }
  console.log(userArray);
}

const generator = document.querySelector(".users-generator");

generator.addEventListener("click", usersGenerator);

// User buttons
const userNames = document.querySelectorAll("[data-name]");
const userAge = document.querySelector(".age");
userNames.forEach((userName) => {
  userName.addEventListener("click", createUser);
});

const usersData = [];
const submitButton = document.querySelector(".send");
let userId = 1;

function createUser(e) {
  if (+userAge.value < 18 || +userAge.value > 40) {
    alert("only 18 - 40 age range");
    return false;
  }
  let userName = e.target;
  let userData = {};
  userData.name = userName.dataset.name;
  userData.age = +userAge.value;
  userData.id = userId;
  userId++;
  let userCard = document.createElement("div");
  userCard.classList.add("user-card");
  userCard.innerHTML = `<span>name: ${userData.name}</span><span>age: ${userData.age}</span><span>id: ${userData.id}</span>`;
  document.querySelector(".users").appendChild(userCard);
  usersData.push(userData);
}

submitButton.addEventListener("click", req);

function req() {
  sendData("http://localhost:3000/database", usersData).catch((err) =>
    console.error("ERROR")
  );
}

async function sendData(url, data) {
  const res = await fetch(`${url}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(data),
  });
  if (!res.ok) {
    throw new Error(`Ошибка ${url}, ${res.status}`);
  }
  return await res.json();
}
