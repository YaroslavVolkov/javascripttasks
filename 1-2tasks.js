// First task
const array = [1, 2, 3, 4, 5, "x", "X", "A", "B"];
const findIndexWithArray = (array, elementToFind) =>
  array.indexOf(elementToFind);

console.log(findIndexWithArray(array, "B"));

const findIndexWithFor = (array, elementToFind) => {
  for (let i = 0; i < array.length; i++) {
    if (array[i] === elementToFind) {
      return i;
    }
  }
};

console.log(findIndexWithFor(array, "A"));

// Second task
const array2 = [
  {
    id: 1,
    name: "test",
    createdAt: "2021-04-21",
  },
  {
    id: 2,
    name: "x",
    createdAt: "2021-04-20",
  },
  {
    id: "X",
    name: "X",
    createdAt: "2021-04-20",
  },
  {
    id: 22,
    name: "cat",
    createdAt: "2021-04-20",
  },
];

const findElementWithFind = (array2, idToFind) =>
  array2.find((el) => el.id == idToFind);

console.log(findElementWithFind(array2, 22));

const findElementWithFor = (array2, idToFind) => {
  for (let i = 0; i < array2.length; i++) {
    if (array2[i].id == idToFind) {
      return array2[i];
    }
  }
};

console.log(findElementWithFor(array2, "X"));